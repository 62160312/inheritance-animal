/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.inheritance;

/**
 *
 * @author ASUS
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal ("Ani","white",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog ("Dang","Black&White" );
        dang.speak();
        dang.walk();
        
        Dog to = new Dog ("To","Brown");
        to.walk();
        to.speak();
        
        Dog mome = new Dog ("Mome","White");
        to.walk();
        to.speak();
        
        Dog bat = new Dog ("Bat","White");
        to.walk();
        to.speak();
        
        
        Cat zero = new Cat ("Zero", "Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom","Orange");
        zom.speak();
        zom.walk();
        zom.fiy();
        
        System.out.println("Zom is Animal: "+ (zom instanceof Animal));
        System.out.println("Zom is Duck: "+ (zom instanceof Duck));
        System.out.println("Zom is Cat: "+ (zom instanceof Object));
        System.out.println("Animal is Dog: "+ (animal instanceof Dog));
        System.out.println("Animal is Animal: "+ (animal instanceof Animal));
        System.out.println("To is Dog: "+ (to instanceof Dog));
        
        Animal ani1 = null;
        Animal ani2 = zero;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1: zom is Duck "+(ani1 instanceof Duck));
        
        Animal[] animals = {dang, zero, zom, to, mome, bat};
        for(int i = 0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
               if(animals[i] instanceof Duck){
                   Duck duck = (Duck)animals[i];
                   duck.fiy();
               }
        }
        
    }
}
