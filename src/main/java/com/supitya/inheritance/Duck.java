/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.inheritance;

/**
 *
 * @author ASUS
 */
public class Duck extends Animal {

    private int numberOfWings;

    public Duck(String name, String color) {
        super(name, color, 2);
        System.out.println("Duck created");
    }

    @Override
    public void walk() {
        System.out.println("Duck:" + name + " walk with " + numberOfLegs + " leg.");
    }

    @Override
    public void speak() {
        System.out.println("Cat:" + name + " speak > Gab Gab!!!");

    }

    public void fiy() {
        System.out.println("Duck : " + name + " fly!!!");
    }
}
